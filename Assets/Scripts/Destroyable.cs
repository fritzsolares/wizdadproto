using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Destroyable : Entity
{
    public SpriteRenderer Tintable;
    private Color HealthyColor;
    public Color DeathlyColor;

    public float SelfDamagePerMS = 0f;
    private float NextDamageTime = 0f;

    public float PauseBeforeDestroy = 0f;

    public bool ExplodesOnDestroy = false;
    public GameObject ExplosionBulletTemplate;
    public GameObject Explosion;
    
    public float RadialDamageDamage = 0f;
    public float RadialDamageRadius = 0f;


    public float PlayerContactDamage = 0f;
    
    
   
    protected override void OnStart()
    {
        base.OnStart();
        if (Tintable!=null) HealthyColor = Tintable.color;
        TintHealth();

        if (RadialDamageDamage > 0f)
        {
            var  colliders=Physics2D.OverlapCircleAll(transform.position, RadialDamageRadius);
            foreach (var c in colliders)
            {
             
                if (c.gameObject.GetComponent<Entity>())
                {
                    var distance = Vector3.Distance(c.transform.position, transform.position);
                    var fraction = 1f-(distance / RadialDamageDamage);
                    c.gameObject.GetComponent<Entity>().Damage(RadialDamageDamage*fraction);
                }
                
            }
        }
        
        if (Explosion!=null) Explosion.SetActive(false);
    }

    protected override void OnUpdate()
    {
        if (SelfDamagePerMS>0 && Time.time>NextDamageTime)
        {
            Damage(SelfDamagePerMS);
            NextDamageTime = Time.time + 0.01f;
        }
    }
     
     

    protected override void OnDamage(float effectiveDamage)
    {
        if (!IsAlive) return;
        TintHealth();
    }

    public void TintHealth()
    {
        if (Tintable ==null) return;
        var healthFraction = Health / MaxHealth;
        Tintable.color = Color.Lerp(DeathlyColor,HealthyColor, healthFraction);
    }
    
    
    
    protected override void OnKill()
    {
    
        if (!ExplodesOnDestroy) return;


        if (ExplosionBulletTemplate != null)
        {
            int num = 30;

            for (int i = 0; i < num; i++)
            {

                var bullet = Instantiate(ExplosionBulletTemplate).GetComponent<Bullet>();
                bullet.gameObject.SetActive(true);

                var angle = 360f * (float) i / (float) num;

                var mag = 0.5f;
                var offset = new Vector3(Mathf.Sin(angle) * mag, Mathf.Cos(angle) * mag);

                bullet.Shoot(transform.position + offset, angle, Body.velocity);
                var size = 0.5f + Random.value * 1.0f;
                bullet.transform.localScale *= size;
                bullet.Body.mass *= size * size;
                bullet.AgeSeconds *= size;
            }
        }

        if (Explosion != null)
        {
            Explosion.transform.SetParent(this.transform.parent);
            Explosion.SetActive(true);
        }
        
    }
    
}

    