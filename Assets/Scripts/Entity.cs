using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using UnityEditor;
using UnityEngine;
using UnityEngine.XR.LegacyInputHelpers;

public class Entity : MonoBehaviour
{
    // Start is called before the first frame update
    
    public float Speed;
    public float Armor = 1f;
    public float Health = 100;
    public float MaxHealth = 100;
    public Rigidbody2D Body;
    public bool IsAlive = true;
    public bool AbsorbsBullets = true;


    void Awake()
    {
        Body = GetComponentInChildren<Rigidbody2D>();
        OnStart();
    }

    protected virtual void OnStart() { }
    protected virtual void  OnUpdate() {}
    protected virtual void  OnKill() {}
    

    protected virtual void OnDamage(float effectiveDamage) { }

    public float Damage(float damage)
    {
        if (!IsAlive) return 0f;
        
        var origHealth = Health;
        var effectiveDamage = damage * Armor;
        Health -= effectiveDamage;
        if (Health <= 0)
        {
            Kill();
            return (origHealth);
        }
        else
        {
            OnDamage(effectiveDamage);
            return effectiveDamage;
        }
        
    }

    public void Kill()
    {
        IsAlive = false;
        OnKill();
        Destroy(gameObject);
    }


    void FixedUpdate()
    {
        if (IsAlive) OnUpdate();
    }

    public static float Angle(Vector2 v)
    {
        if (v.x < 0)
        {
            return 360f - (Mathf.Atan2(v.x, v.y) * Mathf.Rad2Deg * -1f);
        }
        else
        {
            return Mathf.Atan2(v.x, v.y) * Mathf.Rad2Deg;
        }
    }

    
    
}

