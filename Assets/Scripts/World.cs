using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using Random = UnityEngine.Random;

public class World : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject RoomTemplate;
    public GameObject MobTemplate;
    public GameObject BarrelTemplate;
    public GameObject TreeTemplate;

    public Transform Scenery;
    
    public int Width = 15;
    public int Height = 15;

    void Start()
    {


        Width = 0;
        Height = 0;
        
        for (var i = 0; i < Width; i++)
        for (var j = 0; j < Height; j++)
        {
            var obj = Instantiate(RoomTemplate);
            obj.transform.parent = Scenery;
            obj.transform.position = new Vector3((i-Width/2f)*10f,(j-Height/2f)*10f, 0f);
            var leftWall = obj.transform.Find("LeftWall").gameObject;
            var bottomWall = obj.transform.Find("BottomWall").gameObject;
            var leftDoor = obj.transform.Find("LeftDoor").gameObject;
            var bottomDoor = obj.transform.Find("BottomDoor").gameObject;

            var r = Random.value;
            
             
            
            if (r < 0.1f)
            {
                leftWall.SetActive(true);
                leftDoor.SetActive(false);
            } else if (r < 0.2f)
            {
                leftWall.SetActive(false);
                leftDoor.SetActive(true);
            }
            else
            {
                leftWall.SetActive(false);
                leftDoor.SetActive(false);
            }
            
            if (r < 0.1f)
            {
                bottomWall.SetActive(true);
                bottomDoor.SetActive(false);
            } else if (r < 0.2f)
            {
                bottomWall.SetActive(false);
                bottomDoor.SetActive(true);
            }
            else
            {
                bottomWall.SetActive(false);
                bottomDoor.SetActive(false);
            }

            
            
            var barrel1 = obj.transform.Find("Barrel1").gameObject;
            var barrel2 = obj.transform.Find("Barrel2").gameObject;
            barrel1.SetActive(Random.value<0.1f);
            barrel2.SetActive(Random.value<0.1f);
            

        }
        RoomTemplate.SetActive(false);
        
        for (var i = 0; i < 20; i++)
        {
            var obj = Instantiate(MobTemplate);
            do {
                obj.transform.position = Vector2.right*0f+Random.insideUnitCircle * 20f;

            } while (obj.transform.position.magnitude<5f);

            obj.GetComponent<Entity>().Speed *= (0.6f + Random.value * 0.4f);
        }
        MobTemplate.SetActive(false);
        
        BarrelTemplate.SetActive(false);
        /*
        
        for (var i = 0; i < 20; i++)
        {
            var obj = Instantiate(BarrelTemplate);
            obj.transform.position = Vector2.right*0f+Random.insideUnitCircle * 20f;
            
            
        }
        BarrelTemplate.SetActive(false);
        
        
        /*&
        for (var i = 0; i < 400; i++)
        {
            var obj = Instantiate(TreeTemplate);
            
            obj.transform.position = Random.insideUnitCircle * 100f;
            var scale = 1f + Random.value * 0.5f;
            obj.transform.localScale *= scale*scale;
            obj.GetComponent<Destroyable>().Health *= Random.value;
            obj.GetComponent<Destroyable>().TintHealth();
        }
        
        */
        /*
        for (var i = 0; i < 20; i++)
        for (var j = 0; j < 20; j++)
        {
            var obj = Instantiate(TreeTemplate);
            var ii = i - 10;
            var jj = j - 10;
            var offsetX = jj % 2 == 0 ? 0f : 5f;
            obj.transform.position = new Vector3(offsetX+(ii - Width / 2f) * 10f, (jj - Height / 2f) * 10f, 0f);
            var scale = 1f + Random.value * 0.5f;
            obj.transform.localScale *= scale*scale;
            obj.GetComponent<Destroyable>().Health *= Random.value;
            obj.GetComponent<Destroyable>().TintHealth();
        }
*/

    }

  
}




