using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class Bullet : Entity
{

    public float DamageApplied = 50f;
    public float LifeSeconds = 0.5f; //
    public float AgeSeconds = 0;
   
    public Transform Image;
    public TrailRenderer Trail;
    
    
   
    void FixedUpdate()
    {
        AgeSeconds += Time.fixedUnscaledDeltaTime;
        if (IsAlive && AgeSeconds > LifeSeconds)
        {
            Kill();
        }
    }
    
    public void Shoot(Vector3 position, float angle, Vector2 velocity=new Vector2())
    {
        transform.position = position;
        var direction =  Quaternion.Euler(0, 0, -angle) * Vector3.up;
        
        Body.AddForce(Speed * direction.normalized);
        Body.velocity += velocity;    
    }

    private void OnCollisionEnter2D(Collision2D otherCollision)
    {
        if (!IsAlive) return;
        var otherEntity = otherCollision.gameObject.GetComponent<Entity>();
        if (otherEntity == null || !otherEntity.IsAlive) return;

        if (otherEntity.CompareTag("Bullet")) return;
        
        var willDestroy = (otherEntity.AbsorbsBullets);
        
        if (otherEntity!=null)
            otherEntity.Damage(DamageApplied);
        
        if (willDestroy)
        {
            IsAlive = false;
            gameObject.SetActive(false);
        }
        
   
    }
}
