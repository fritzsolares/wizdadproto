using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public static Game Instance;
    
    public Player Player;

    void Awake()
    {
        Instance = this;
    }
    
    void Start()
    {
        Physics2D.gravity = Vector2.zero;
    }

    private void Update()
    {
        Camera.main.orthographicSize +=0.0005f;//*= 1.0001f;
    }
}


