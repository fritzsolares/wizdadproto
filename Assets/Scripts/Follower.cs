using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Follower : Destroyable
{
    public Entity Target;

    private void FixedUpdate()
    {
        if (!IsAlive) return;
        if (!Target) return;
        var dpos = Target.transform.position - transform.position;
        if (dpos.magnitude > 50f) return;
        dpos.z = 0;
        transform.up = dpos;
/*
        var hits = Physics2D.CircleCastAll(transform.position,;
        foreach (var hit in hits)
        {
            var obj = hit.collider.gameObject;
            if (obj != gameObject)
            {
                
            }
            if (CompareTag(.hit.collider)
        }

*/
        Body.AddForce(Speed*Time.fixedUnscaledDeltaTime*dpos.normalized,ForceMode2D.Impulse);
        
        
        
    }
}
