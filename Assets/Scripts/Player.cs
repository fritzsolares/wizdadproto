using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class Player : Entity
{
   
    public Transform Image;
    public Camera Camera;
   
    public Transform BulletOrigin;
    public GameObject BulletTemplate;

    public float FireIntervalSeconds = 0.2f;
    private float nextFireTime = 0f;
    
    override protected void OnStart()
    {
        Camera=Camera.main;
        BulletTemplate.SetActive(false);
    }

    protected override void OnUpdate()
    {
        var direction = Camera.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        direction.z = 0;
        Image.transform.up = direction;
        var input = Vector2.zero;
        input.x = Input.GetKey(KeyCode.A) ? -1f : Input.GetKey(KeyCode.D) ? +1f : 0;
        input.y = Input.GetKey(KeyCode.S) ? -1f : Input.GetKey(KeyCode.W) ? +1f : 0;
        Body.AddForce(Speed*Time.fixedUnscaledDeltaTime*input.normalized,ForceMode2D.Impulse);
    }

    void Update()
    {
        var pos = transform.position;
        pos.z = Camera.transform.position.z;
        Camera.transform.position = pos;
        if (Input.GetMouseButton(0)) TryFire();
    }

    private void TryFire()
    {
        if (Time.time < nextFireTime) return;
    
        Fire(Random.value*10f);
    
        nextFireTime = Time.time + FireIntervalSeconds;
    }

    private void Fire(float angleAdjust)
    {
        var bullet = GameObject.Instantiate(BulletTemplate);
        bullet.SetActive(true);
        
       
        Vector2 offset= Camera.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        var angle = Angle(offset);
        angle += angleAdjust;
        var origin = BulletOrigin.position;
        bullet.GetComponent<Bullet>().Shoot(origin, angle, Body.velocity);

    }
    
    private void OnCollisionEnter2D(Collision2D otherCollision)
    {
        if (!IsAlive) return;
        
        var destroyable = otherCollision.gameObject.GetComponent<Destroyable>();
        if (destroyable == null || !destroyable.IsAlive) return;

        if (destroyable.PlayerContactDamage > 0)
            Damage(destroyable.PlayerContactDamage);

    }

    

}
