using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FilledBar : MonoBehaviour
{
    public Image HealthBarImage;

    private float TargetValue;

    public void SetHealthBarValue(float value, bool noLerp)
    {
        TargetValue = value;
        if (noLerp) HealthBarImage.fillAmount = TargetValue;
    }

    void Update()
    {
        HealthBarImage.fillAmount = Mathf.Lerp(HealthBarImage.fillAmount, TargetValue, Time.deltaTime*3f);
        if(TargetValue < 0.2f)
        {
            SetHealthBarColor(Color.red);
        }
        else
        {
            SetHealthBarColor(Color.white);
        }
    }

    public void SetHealthBarColor(Color healthColor)
    {
        HealthBarImage.color = healthColor;
    }
 

}
