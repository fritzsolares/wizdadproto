using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScreen : MonoBehaviour
{

    public FilledBar Health;
    
    void Start()
    {
        Health.SetHealthBarValue(Game.Instance.Player.Health/Game.Instance.Player.MaxHealth,true);
    }

    // Update is called once per frame
    void Update()
    {
        Health.SetHealthBarValue(Game.Instance.Player.Health/Game.Instance.Player.MaxHealth,false);
    }
}
